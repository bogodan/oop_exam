#include <iostream>
#include "sequence/sequence.h"


template<typename T>
void out_vec(std::vector<T> vec){
    std::cout << "[ ";
    for(const auto& elem: vec){
        std::cout << elem << " ";
    }
    std::cout << "]";
}

int main() {

    std::cout << "Hello in another brunch! abr" << std::endl;
    std::cout << "Enter count of numbers: ";
    sequence seq = sequence::generate_sequence();
    std::vector<int> res = seq.increasing_subsequence();
    std::cout << std::endl;
    out_vec(res);
    std::cout << std::endl;
    seq.out_results();
    return 0;
}
