//
// Created by bogodan on 15.06.21.
//

#include "sequence.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

TEST(TestSequence, TestEmptySeq){
    auto seq = sequence(std::vector<int>());
    EXPECT_TRUE(seq.increasing_subsequence().empty());
}

TEST(TestSequence, TestDecrease){
    auto seq = sequence(std::vector<int>{5,4,3,2,1});
    EXPECT_EQ(seq.increasing_subsequence().size(), 1);
}


TEST(TestSequence, TestIncrease){
    auto seq = sequence(std::vector<int>{1,5,1,2,3,4,5,4,5});
    std::vector<int> expectedVec = {1,2,3,4,5};
    EXPECT_EQ(seq.increasing_subsequence(), expectedVec);
}
