//
// Created by bogodan on 15.06.21.
//

#include <chrono>
#include "sequence.h"

sequence sequence::generate_sequence() {

    srand(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()));
    sequence res;
    int count_number;
    std::cin >> count_number;
    for(int i = 0; i< count_number; i++){
        int elem = rand()%m_module+1;
        std::cout << elem << " ";
        res.m_vec.push_back(elem);
    }


    return res;
}

int sequence::increasing_subsequence_len() const {


    return 0;
}

std::vector<int> sequence::increasing_subsequence() const {
    std::vector<int> res;
    if (m_vec.empty()){
        return std::vector<int>();

    }
    int max_count_symbols=1;
    int count_symbols = 1;

    auto it_on_start = m_vec.begin();
    auto it_on_end = m_vec.begin()+1;

    auto it_on_start_max = it_on_start;
    while((it_on_end != m_vec.end()) ){

        if(*it_on_end > *(it_on_end-1)) {
            count_symbols++;
            it_on_end++;
            continue;
        } else{
            if(count_symbols>max_count_symbols){
                max_count_symbols= count_symbols;
                it_on_start_max = it_on_start;
            }
            it_on_start = it_on_end;
            it_on_end = it_on_start+1;
            count_symbols = 1;
        }


    }
    return std::vector<int>(it_on_start_max, it_on_start_max+max_count_symbols);



}

void sequence::out_results() const {

    std::vector<int> in_seq = increasing_subsequence();

    int res_len = in_seq.size();
    int res_product=1;
    for(const auto& el: in_seq){
        res_product *= el;
    }
    std::cout << "count numberts in subsequence: " << res_len << std::endl;
    std::cout << "product numberts in subsequence: " << res_product << std::endl;


}

sequence::sequence(std::vector<int> vec):m_vec(vec) {

}
