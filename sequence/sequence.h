//
// Created by bogodan on 15.06.21.
//

#ifndef OOP_EXAM_SEQUENCE_H
#define OOP_EXAM_SEQUENCE_H

#include <vector>
#include <iostream>
#include <random>



class sequence {
private:
std::vector<int> m_vec;
static const int m_module = 5;

public:
    sequence() = default;
    sequence(std::vector<int> vec);
    static sequence generate_sequence();
    void out_results() const;
//    sequence();
    int increasing_subsequence_len() const;
    std::vector<int> increasing_subsequence() const;

};


#endif //OOP_EXAM_SEQUENCE_H
